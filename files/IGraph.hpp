//
// IGraph.hpp for graph in /home/charti_t/test/snake/sdl
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Wed Apr  1 16:13:56 2015 Thomas Chartier
// Last update Sun Apr  5 17:43:13 2015 Bengler Bastien
//

#ifndef IGRAPH_H_
# define IGRAPH_H_

#include <vector>
#include "nibbler.hpp"

class IGraph
{
public:
  virtual void initLib() = 0;
  virtual void showWindow(const int, const int) = 0;
  virtual void printMap(const std::vector<int>, const int) = 0;
  virtual void uptWindow() = 0;
  virtual int getKey() const = 0;
protected:
  CoreGame	*_core;

};

typedef IGraph *(*fptr)(CoreGame* core);

#endif /* !IGRAPH_H_ */
