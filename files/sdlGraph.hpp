//
// sdlGraph.hpp for graph in /home/charti_t/test/snake/sdl
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Wed Apr  1 16:13:42 2015 Thomas Chartier
// Last update Sun Apr  5 17:21:15 2015 Bengler Bastien
//

#ifndef SDLGRAPH_H_
# define SDLGRAPH_H_

#include <SDL/SDL.h>
#include "IGraph.hpp"

class sdlGraph : public IGraph
{
public:
  sdlGraph(CoreGame *core);
  virtual ~sdlGraph();

  virtual void initLib();
  virtual void showWindow(const int, const int);
  void printSnake(const int,const int, const int, const int);
  void printApple(const int, const int, const int, const int);
  virtual int getKey() const;
  virtual void printMap(const std::vector<int>, const int);
  virtual void uptWindow();
private:
  SDL_Surface	*window;
  SDL_Surface	*snake;
  SDL_Surface	*back;
  SDL_Surface	*apple;
  SDL_Rect      applePos;
  SDL_Rect	snakePos;
  SDL_Rect	backPos;
  SDL_Event	event;
  int		lWin;
  int		hWin;
};

# define SIZE_CELL 10

#endif /* !SDLGRAPH_H_ */
