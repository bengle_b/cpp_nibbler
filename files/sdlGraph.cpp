//
// sdlGraph.cpp for graph in /home/charti_t/test/snake/sdl
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Wed Apr  1 16:13:38 2015 Thomas Chartier
// Last update Sun Apr  5 17:30:17 2015 Bengler Bastien
//
#include <vector>
#include <iostream>
#include "sdlGraph.hpp"
#include "nibbler.hpp"

sdlGraph::sdlGraph(CoreGame *core)
{
  _core = core;
}

sdlGraph::~sdlGraph()
{
  SDL_FreeSurface(window);
  SDL_FreeSurface(snake);
  SDL_FreeSurface(back);
  if (_core)
    delete _core;
}

void	sdlGraph::initLib()
{
  SDL_Init(SDL_INIT_VIDEO);
}

void	sdlGraph::showWindow(const int height, const int width)
{
  this->lWin = width;
  this->hWin = height;
  window = SDL_SetVideoMode(height,width,32,SDL_HWSURFACE);
  SDL_WM_SetCaption("nibbler - SDL", NULL);
}

void	sdlGraph::printSnake(const int x, const int y, const int height, const int weight)
{
  snake = SDL_CreateRGBSurface(SDL_HWSURFACE, height, weight, 32,0,0,0,0);
  SDL_FillRect(snake, NULL, SDL_MapRGB(snake->format,0,255,0));
  snakePos.x = x;
  snakePos.y = y;
  SDL_BlitSurface(snake,NULL,window,&snakePos);
  SDL_Flip(window);
}

void	sdlGraph::printApple(const int x, const int y, const int height, const int weight)
{
  apple = SDL_CreateRGBSurface(SDL_HWSURFACE, height, weight, 32,0,0,0,0);
  SDL_FillRect(apple, NULL, SDL_MapRGB(apple->format,255,0,0));
  applePos.x = x;
  applePos.y = y;
  SDL_BlitSurface(apple,NULL,window,&applePos);
  SDL_Flip(window);
}

int	sdlGraph::getKey() const
{  
  SDL_Event event;

  SDL_PollEvent(&event);
  if (event.type == SDL_KEYDOWN)
    {
      if (event.key.keysym.sym == SDLK_DOWN)
	_core->setDirection(DOWN);
      else if (event.key.keysym.sym == SDLK_RIGHT)
	_core->setDirection(RIGHT);
      else if (event.key.keysym.sym == SDLK_LEFT)
	_core->setDirection(LEFT);
      else if (event.key.keysym.sym == SDLK_UP)
	_core->setDirection(UP);
      else if (event.key.keysym.sym == SDLK_ESCAPE)
      	return (5);
    }
  return (0);
}

void	sdlGraph::printMap(const std::vector<int> tab, const int limitWidth)
{
  int	i = 0;
  int	x = 0;
  int	y = 0;
  int	count = 0;

  while(i < tab.size())
    {
      while (count < limitWidth)
	{
	  if (i == tab.size())
	    break;
	  if ((int)tab.at(i) >= 0 || (int)tab.at(i) == -2)
	    printSnake(x, y, SIZE_CELL, SIZE_CELL);
	  if ((int)tab.at(i) == -3)
	    printApple(x, y, SIZE_CELL, SIZE_CELL);
	  x += SIZE_CELL;
	  i++;
	  count += 1;
	}
      x = 0;
      y += SIZE_CELL;
      count = 0;
    }
}

void	sdlGraph::uptWindow()
{
  back = SDL_CreateRGBSurface(SDL_HWSURFACE, hWin, lWin, 32,0,0,0,0);
  SDL_FillRect(back, NULL, SDL_MapRGB(back->format,0,0,0));
  backPos.x = 0;
  backPos.y = 0;
  SDL_BlitSurface(back,NULL,window,&backPos);
  SDL_Flip(window);
}

extern "C"
{
  sdlGraph	*lib_nibbler_sdl(CoreGame *core)
  {
    return new sdlGraph(core);
  }
}
