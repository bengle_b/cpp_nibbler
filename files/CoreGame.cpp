//
// CoreGame.cpp for CoreGame in /home/bengle_b/rendu/cpp_nibbler
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Tue Mar 24 16:44:58 2015 Bengler Bastien
// Last update Sun Apr  5 17:42:05 2015 Bengler Bastien
//

#include <iostream>
#include <stdexcept>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include "nibbler.hpp"
#include "IGraph.hpp"

CoreGame::CoreGame(const int x, const int y)
{
  _width = x / 10;
  _height = y / 10;
  _gameBoard = std::vector<int>(_width * _height, -1);
  _keydirection = RIGHT;
  fptr_direction[0] = &CoreGame::moveUp;
  fptr_direction[1] = &CoreGame::moveDown;
  fptr_direction[2] = &CoreGame::moveRight;
  fptr_direction[3] = &CoreGame::moveLeft;
  placeSnake();
}

CoreGame::~CoreGame()
{
}

CoreGame::CoreGame(const CoreGame &core)
{
  _width = core.getWidth();
  _height = core.getHeight();
  _gameBoard = core.getGameBoard();
  _keydirection = core.getDirection();
}

CoreGame&	CoreGame::operator=(const CoreGame &core)
{
  if (&core != this)
    {
      _gameBoard = core.getGameBoard();
      _keydirection = core.getDirection();
      _width = core.getWidth();
      _height = core.getHeight();
    }
  return *this;
}

const std::vector<int>&	CoreGame::getGameBoard() const
{
  return _gameBoard;
}

int	CoreGame::getWidth() const
{
  return _width;
}

int	CoreGame::getHeight() const
{
  return _height;
}

void	CoreGame::setQueue(const int queue)
{
  _queue = queue;
}

int	CoreGame::getQueue() const
{
  return _queue;
}

int	CoreGame::getHead() const
{
  int	i;

  i = getQueue();
  while (_gameBoard.at(i) != -2)
    i = _gameBoard.at(i);
  return (i);
}

int	CoreGame::getApples() const
{
  unsigned int	i = 0;
  int		apple = 0;

  while (i < _gameBoard.size())
    {
      if (_gameBoard.at(i) == -3)
	++apple;
      ++i;
    }
  return (apple);
}

void	CoreGame::setDirection(const DIRECTION keydirection)
{
  _keydirection = keydirection;
}

DIRECTION	CoreGame::getDirection() const
{
  return _keydirection;
}

void		CoreGame::getMove()
{
  int	pos;

  srand(time(NULL) % getppid());
  while (getApples() == 0)
    {
      if ((_gameBoard.at(pos = rand() % (_width * _height))) == -1)
	_gameBoard.at(pos) = -3;
    }
  (this->*fptr_direction[getDirection()])();
}

void	CoreGame::placeSnake()
{
  setQueue(getHeight() + getWidth() / 2 - 2);
  _gameBoard.at(getHeight() + getWidth() / 2 - 2) = getHeight() + getWidth() / 2 - 1;
  _gameBoard.at(getHeight() + getWidth() / 2 - 1) = getHeight() + getWidth() / 2;
  _gameBoard.at(getHeight() + getWidth() / 2) = getHeight() + getWidth() / 2 + 1;
  _gameBoard.at(getHeight() + getWidth() / 2 + 1) = -2;
}

void	CoreGame::moveUp()
{
  int	head;
  int	old_queue;

  head = getHead();
  old_queue = getQueue();
  if (head >= _width && _gameBoard.at(head - _width) == -1)
    {
      _gameBoard.at(head) = head - _width;
      _gameBoard.at(head - _width) = -2;
      setQueue(_gameBoard.at(_queue));
      _gameBoard.at(old_queue) = -1;
    }
  else if (head >= _width && _gameBoard.at(head - _width) == -3)
    {
      _gameBoard.at(head) = head - _width;
      _gameBoard.at(head - _width) = -2;
    }
  else
    throw std::runtime_error("Game Over, you couldn't move upper.");
}

void	CoreGame::moveDown()
{
  int	head;
  int	old_queue;

  head = getHead();
  old_queue = getQueue();
  if (head < ((_width * _height) - _width) &&
      _gameBoard.at(head + _width) == -1)
    {
      _gameBoard.at(head) = head + _width;
      _gameBoard.at(head + _width) = -2;
      setQueue(_gameBoard.at(_queue));
      _gameBoard.at(old_queue) = -1;
    }
  else if (head < (_width * _height) - _width &&
	   _gameBoard.at(head + _width) == -3)
    {
      _gameBoard.at(head) = head + _width;
      _gameBoard.at(head + _width) = -2;
    }
  else
    throw std::runtime_error("Game Over, you couldn't move lower.");
}

void	CoreGame::moveRight()
{
  int	head;
  int	old_queue;

  head = getHead();
  old_queue = getQueue();
  if (head != (_width * _height) && ((head + 1) % _width != 0))
    {
      _gameBoard.at(head) = head + 1;
      _gameBoard.at(head + 1) = -2;
      setQueue(_gameBoard.at(_queue));
      _gameBoard.at(old_queue) = -1;
    }
  else if (_gameBoard.at(head + 1) == -3)
    {
      _gameBoard.at(head) = head + 1;
      _gameBoard.at(head + 1) = -2;
    }
  else
    throw std::runtime_error("Game Over, you couldn't move to the right.");
}

void	CoreGame::moveLeft()
{
  int	head;
  int	old_queue;

  head = getHead();
  old_queue = getQueue();
  if (head != 0 && head % _width > 0)
    {
      _gameBoard.at(head) = head - 1;
      _gameBoard.at(head - 1) = -2;
      setQueue(_gameBoard.at(_queue));
      _gameBoard.at(old_queue) = -1;
    }
  else if (_gameBoard.at(head - 1) == -3)
    {
      _gameBoard.at(head) = head - 1;
      _gameBoard.at(head - 1) = -2;
    }
  else
    throw std::runtime_error("Game Over, you couldn't move to the left.");
}
