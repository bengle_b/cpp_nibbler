/*
** nibbler.h for nibbler in /home/bengle_b/rendu/cpp_nibbler/files
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Tue Mar 24 16:42:46 2015 Bengler Bastien
// Last update Sun Apr  5 17:42:15 2015 Bengler Bastien
*/

#ifndef NIBBLER_HPP_
# define NIBBLER_HPP_

#include <vector>

typedef enum	DIRECTION
  {
    UP,
    DOWN,
    RIGHT,
    LEFT
  }		DIRECTION;

class	CoreGame
{
 public:
  CoreGame(const int x, const int y);
  ~CoreGame();
  CoreGame(const CoreGame&);
  CoreGame& operator=(const CoreGame&);

  const std::vector<int>& getGameBoard() const;
  int	getWidth() const;
  int	getHeight() const;

  void	setQueue(const int queue);
  int	getQueue() const;

  int	getHead() const;

  int	getApples() const;

  void	setDirection(const DIRECTION);
  DIRECTION	getDirection() const;

  void	getMove();

  void	placeSnake();

  void	moveUp();
  void	moveDown();
  void	moveRight();
  void	moveLeft();

private:
  int	_width;
  int	_height;
  int	_queue;
  std::vector<int> _gameBoard;
  DIRECTION	_keydirection;
  void	(CoreGame::*fptr_direction[4])();
};

#endif /* !NIBBLER_H_ */
