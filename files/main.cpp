//
// main.cpp for main in /home/bengle_b/rendu/cpp_nibbler/files
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Tue Mar 24 16:41:10 2015 Bengler Bastien
// Last update Sun Apr  5 17:42:39 2015 Bengler Bastien
//

#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>
#include "nibbler.hpp"
#include "IGraph.hpp"

int		main(int ac, char **av)
{
  void		*handle;
  std::string	name_lib;
  unsigned int	pos;
  void		*mkr;
  fptr		constructor_ptr;
  IGraph	*libGraph;

  try
    {
      if (ac != 4 || atoi(av[1]) < 500 || atoi(av[2]) < 500
	  || atoi(av[1]) > 1000 || atoi(av[2]) > 1000)
	throw std::invalid_argument("Usage: ./nibbler Xvalue Yvalue [LIB.so]");
      name_lib = av[3];
      name_lib.insert(0, "./");
      handle = dlopen(name_lib.c_str(), RTLD_LAZY);
      if (dlerror() != NULL)
	throw std::invalid_argument("Can not open the dynamic library");
      name_lib = av[3];
      if ((pos = name_lib.find(".", 0)) > name_lib.size())
	throw std::invalid_argument("Invalid dynamic library. Please add .so extension");
      name_lib.at(pos) = 0;
      mkr = dlsym(handle, name_lib.c_str());
      if (dlerror() != NULL)
      	throw std::invalid_argument("Can not find the constructor's function");
    }
  catch (std::invalid_argument &e)
    {
      std::cout << e.what() << std::endl << std::endl;
      return (-1);
    }
  CoreGame	*game = new CoreGame(atoi(av[1]), atoi(av[2]));

  constructor_ptr = (fptr)mkr;
  libGraph = constructor_ptr(game);

  libGraph->initLib();
  libGraph->showWindow(atoi(av[1]), atoi(av[2]));

  while (1)
    {
      libGraph->printMap(game->getGameBoard(), atoi(av[1])/10);
      if (libGraph->getKey() == 5)
	break;
      try
	{
	  game->getMove();
	}
      catch (std::runtime_error &e)
	{
	  std::cout << e.what() << std::endl << std::endl;
	  return (-1);
	}
      catch (std::out_of_range &e)
	{
	  std::cout << "... You just eat your self ..." << std::endl << std::endl;
	  return (-1);
	}
      usleep(40000);
      libGraph->uptWindow();
  }
  delete game;
  dlclose(handle);
  return (0);
}
