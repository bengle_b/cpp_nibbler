##
## Makefile for Makefile in /home/bengle_b/rendu/cpp_nibbler
## 
## Made by Bengler Bastien
## Login   <bengle_b@epitech.net>
## 
## Started on  Tue Mar 24 16:40:26 2015 Bengler Bastien
## Last update Sun Apr  5 17:43:23 2015 Bengler Bastien
##

BINARY_NAME	= nibbler

SDL_NAME	= lib_nibbler_sdl.so

SRC_CORE	= files/main.cpp \
		  files/CoreGame.cpp

SRC_SDL		= files/sdlGraph.cpp

OBJ_CORE	= $(SRC_CORE:.cpp=.o)

CC		= g++

RM		= rm -f

CXXFLAGS	= -Wall -Wextra -Werror

all: $(BINARY_NAME)

$(BINARY_NAME): $(OBJ_CORE)
	$(CC) $(OBJ_CORE) -o $(BINARY_NAME) -ldl -rdynamic
	$(CC) -shared -o $(SDL_NAME) $(SRC_SDL) -fPIC -lSDLmain -lSDL

clean:
	$(RM) $(OBJ_CORE)

fclean: clean
	$(RM) $(BINARY_NAME)
	$(RM) $(SDL_NAME)

re: fclean all

.PHONY: clean fclean re all
